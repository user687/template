/*global require*/
(function () {
  'use strict';

  const gulp = require('gulp'),
    del  = require('del'),
    spritesmith = require('gulp.spritesmith'),
    plumber = require('gulp-plumber'),
    imagemin = require('gulp-imagemin'),
    rupture = require('rupture'),
    stylus = require('gulp-stylus'),
    sourcemaps = require('gulp-sourcemaps'),
    gcmq = require('gulp-group-css-media-queries'),
    autoprefixer = require('gulp-autoprefixer'),
    csscomb = require('gulp-csscomb'),
    browserSync = require('browser-sync'),
    pug = require('gulp-pug'),
    filter = require('gulp-filter'),
    notify = require('gulp-notify'),
    beautify = require('gulp-jsbeautifier'),
    concat = require('gulp-concat'),
    zip = require('gulp-zip'),
    sass = require('gulp-sass');
    sass.compiler = require('node-sass');

  let path = {
    vendor: {
      scripts: [
        'node_modules/jquery/dist/jquery.min.js',
      ],
      styles: [

      ]
    }
  };

  gulp.task('browser-sync', () => {
    browserSync({
      server: {
        baseDir: 'dist'
      },
      notify: true,
      // open: false,
      // online: false, // Work Offline Without Internet Connection
      // tunnel: true, tunnel: "projectname", // Demonstration page: http://projectname.localtunnel.me
    })
  });

  gulp.task('delete', async () => {
    const deletedPaths = await del.sync(['./dist/**']);
    console.log('Deleted files and folders:\n', deletedPaths.join('\n'));
  });

  gulp.task('deleteTemp', async () => {
    const deletedPaths = await del.sync(['./dist/styles/temp']);
  });

  gulp.task('sprite',  () => {
    let spriteData = gulp.src('./app/images/sprite/*')
      .pipe(plumber())
      .pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.styl',
        imgPath: '../images/sprite.png',
        cssFormat: 'stylus',
        algorithm: 'binary-tree',
        //retinaImgName: 'sprite2.png',
        //retinaSrcFilter: './app/images/sprite/*2x*',
        padding: 8,
        //engine: 'pngsmith',
        imgOpts: {
          format: 'png'
        }
      }));
    spriteData.img.pipe(gulp.dest('./app/images/'));
    spriteData.css.pipe(gulp.dest('./app/styles/helpers'));
    return spriteData;
  });

  gulp.task('imagemin', () =>
      gulp.src(['./app/images/**/*','!./app/images/sprite/**/*'])
          .pipe(plumber())
          .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
              plugins: [
                {removeViewBox: true},
                {cleanupIDs: false}
              ]
            })
          ],{
            verbose: false
          }))
          .pipe(gulp.dest('./dist/images/'))
  );

  gulp.task('bootstrap', () => {
    return gulp.src('./app/styles/bootstrap.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('./dist/styles/temp'));
  });

  gulp.task('styl',  () => {
    return gulp.src('./app/styles/*.styl')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(stylus({
          compress: false,
          errors: true,
          use: rupture()
        }))
        .pipe(autoprefixer({
          browsers: ['last 3 versions'],
          cascade: false
        }))
        .pipe(gcmq())
        .pipe(csscomb())
        //.pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./dist/styles/temp'));
  });

  gulp.task('stylesconcat', () => {
    return gulp.src([...path.vendor.styles,'./dist/styles/temp/*.css'])
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('./dist/styles'));
  });

  gulp.task('styles', gulp.series(['bootstrap','styl','stylesconcat','deleteTemp']));

  gulp.task('styleDependences', gulp.series(['sprite','imagemin','styles']));

  gulp.task('pug', function () {
    return gulp.src('./app/templates/pages/*.pug')
        .pipe(plumber())
        //.pipe(cached('jade'))
        .pipe(filter(['**', '!*app/templates/pages']))
        .pipe(pug({
          pretty: true
        }))
        // .pipe(prettify({
        //   brace_style: 'expand',
        //   indent_size: 1,
        //   indent_char: '\t',
        //   indent_inner_html: true,
        //   preserve_newlines: true
        // }))
        .pipe(gulp.dest('./dist'))
  });

  gulp.task('scriptsVendor', () => {
    return gulp.src(path.vendor.scripts)
        .pipe(plumber())
        //.pipe(concat('scripts.min.js'))
        // .pipe(uglify()) // Mifify js (opt.)
        .pipe(gulp.dest('./dist/js/vendor'));
  });

  gulp.task('scripts', () => {
    return gulp.src('./app/js/**/*.js')
        .pipe(plumber())
        .pipe(gulp.dest('./dist/js'));

  });

  gulp.task('beautify', () =>
      gulp.src('./dist/js/*.js')
          .pipe(beautify())
          .pipe(gulp.dest('./dist/js'))

  );

  gulp.task('watch', () => {
    gulp.watch('./app/images/sprite/*', gulp.series(['styleDependences'])).on('change', browserSync.reload);
    gulp.watch(['./app/images/**/*','!./app/images/sprite/**/*'], gulp.series(['imagemin'])).on('change', browserSync.reload);
    gulp.watch(['./app/styles/**/*.styl', './app/styles/bootstrap.scss'], gulp.series(['styles'])).on('change', browserSync.reload);
    gulp.watch('./app/js/**/*.js', gulp.series(['scripts','beautify'])).on('change', browserSync.reload);
    gulp.watch('./app/templates/**/*.pug', gulp.series(['pug','beautify'])).on('change', browserSync.reload);
  });

  gulp.task('zip', () =>
      gulp.src('./dist/*')
          .pipe(zip(`archive_${new Date().getDate()}_${(new Date().getMonth()+1+'').padStart(2, '0')}_${new Date().getFullYear()}.zip`))
          .pipe(gulp.dest('./zip'))
  );

  gulp.task('default',  gulp.parallel(gulp.series('pug','scriptsVendor','scripts','styleDependences','beautify', 'browser-sync'),'watch'));
  gulp.task('build', gulp.series('delete','pug','scriptsVendor','scripts','styleDependences','beautify','zip'));




}());
